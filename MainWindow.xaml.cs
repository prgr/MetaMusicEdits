﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Forms;
using TagLib;
using ReplaceWordApp.Models;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using MahApps.Metro.Controls;
using System.Diagnostics;

namespace ReplaceWordApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnPath_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            DialogResult result = dialog.ShowDialog();
            tbFolderPath.Text = dialog.SelectedPath;
            //fill data grid with data
            FillDataGrid(tbFolderPath.Text);
        }

        private void FillDataGrid(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                try
                {
                    var files = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories)
                                                   .Where(s => s.EndsWith(".mp3") || s.EndsWith(".m4a") ||
                                                               s.EndsWith(".acc") || s.EndsWith(".ogg") ||
                                                               s.EndsWith(".flac") || s.EndsWith(".wma"));
                    //create empty collection of dgFiles
                    ObservableCollection<DgFile> dgFiles = new ObservableCollection<DgFile>();
                    int counter = 1;
                    if(files.Count() != 0)
                    {
                        lblEmptyGrid.Content = "";
                        foreach (var filePath in files)
                        {
                            TagLib.File file = TagLib.File.Create(filePath);
                            //get title
                            string title = file.Tag.Title;
                            //if title is empty then set title as file name
                            if (string.IsNullOrEmpty(title))
                                title = filePath.Split('\\').Last();
                            title = title.Replace(".mp3", "").Replace(".Mp3", "");
                            //get artists
                            string[] artists = file.Tag.Performers;
                            //check if there is any artist
                            string artist = "";
                            if (artists.Any())
                                artist = artists[0];
                            DgFile dgFile = new DgFile
                            {
                                DgFileId = counter,
                                Number = counter,
                                Title = title,
                                Artist = artist,
                                Album = file.Tag.Album
                            };
                            //add to collection
                            dgFiles.Add(dgFile);
                            //datagrid
                            counter++;
                        }
                        //fill datagrid
                        dataGrid.ItemsSource = dgFiles;
                        dataGrid.Columns[0].Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("Directory probably does not contain any music files!", "", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private string[] GetFiles(string path)
        {
            try
            {
                //check if path is not empty
                if (!string.IsNullOrEmpty(path))
                {
                    //get files with audio most popular audio extensions
                    var files = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories)
                                                   .Where(s => s.EndsWith(".mp3") || s.EndsWith(".m4a") ||
                                                               s.EndsWith(".acc") || s.EndsWith(".ogg") ||
                                                               s.EndsWith(".flac") || s.EndsWith(".wma"));
                    //check if files are type of any music ext
                    if(files.Count() == 0)
                    {
                        System.Windows.MessageBox.Show("Directory with music files is required!", "", MessageBoxButton.OK, MessageBoxImage.Information);
                        return null;
                    }
                    //for title case
                    TextInfo ti = new CultureInfo("en-US", false).TextInfo;
                    //track counter
                    uint trackCounter = 1;

                    //get datagrid edits
                    List<DgFile> dgFilesEdits = new List<DgFile>();
                    foreach (DgFile editDgFile in dataGrid.ItemsSource)
                    {
                        DgFile newDgFile = new DgFile
                        {
                            Number = editDgFile.Number,
                            Title = editDgFile.Title,
                            DgFileId = editDgFile.DgFileId
                        };
                        dgFilesEdits.Add(newDgFile);
                    }
                    foreach (string filePath in files)
                    {
                        //get file name
                        string fileName = filePath.Split('\\').Last();
                        //initial metadata changes loading the file
                        TagLib.File file = TagLib.File.Create(filePath);
                        //get edited file from datagrid
                        DgFile editedDgFile = dgFilesEdits.Where(x => x.DgFileId == int.Parse(trackCounter.ToString())).FirstOrDefault();
                        //check if album is specified
                        if (string.IsNullOrEmpty(tbAlbumMS.Text))
                        {
                            //get file album
                            string album = file.Tag.Album;
                            //replace unwanted characters
                            //set new album name
                            file.Tag.Album = ti.ToTitleCase(ReplaceCharacters(album));
                            //status --hide
                            //rtbStatusInfo.Document.Blocks.Add(new Paragraph(new Run($"Successfully changed {fileName} album name from {album} to {file.Tag.Album}.")));
                        }
                        else
                            file.Tag.Album = tbAlbumMS.Text;

                        //check if title is not empty if it is then set file name as title
                        if (!string.IsNullOrEmpty(file.Tag.Title))
                        {
                            //get file title
                            string title = file.Tag.Title;
                            if(!string.IsNullOrEmpty(editedDgFile.Title))
                                title = editedDgFile.Title;
                            //replace unwated characters
                            //set new title name
                            file.Tag.Title = ti.ToTitleCase(ReplaceCharacters(title));
                            //status --hide
                            //rtbStatusInfo.Document.Blocks.Add(new Paragraph(new Run($"Successfully changed {title} title name from () to {file.Tag.Title}.")));
                        }
                        else
                        {
                            string newTitle = fileName;
                            if(!string.IsNullOrEmpty(editedDgFile.Title))
                                newTitle = editedDgFile.Title;
                            string extension = fileName.Split('.').Last();
                            newTitle = ti.ToTitleCase(ReplaceCharacters(newTitle));
                            newTitle = newTitle.Replace("." + extension, string.Empty);
                            file.Tag.Title = newTitle;
                        }

                        if (!string.IsNullOrEmpty(tbCoverPath.Text))
                        {
                            IPicture newArt = new Picture(tbCoverPath.Text);
                            file.Tag.Pictures = new IPicture[1] { newArt };
                        }

                        //check if artist is specified
                        if (string.IsNullOrEmpty(tbArtistMS.Text))
                        {
                            //get file artist
                            string artist = file.Tag.Performers[0];
                            //clean all perfmormers
                            file.Tag.Performers[0] = null;
                            //replace unwated characters
                            //set new artist
                            file.Tag.Performers[0] = ti.ToTitleCase(ReplaceCharacters(artist));
                            //status --hide
                            //rtbStatusInfo.Document.Blocks.Add(new Paragraph(new Run($"Successfully changed {fileName} artist name from {artist} to {file.Tag.Performers[0]}.")));
                        }
                        else
                            file.Tag.Performers = new string[] { tbArtistMS.Text.ToString() };

                        if (!string.IsNullOrEmpty(tbYearMS.Text))
                            file.Tag.Year = uint.Parse(tbYearMS.Text);

                        //if checkbox is checked then set track numbers
                        //if(checkBoxSetOrder.IsChecked == true)
                            //file.Tag.Track = trackCounter;

                        trackCounter++;

                        //set track number
                        if (editedDgFile.Number != 0 && editedDgFile != null)
                            file.Tag.Track = uint.Parse(editedDgFile.Number.ToString());

                        //finally save all metadata
                        file.Save();
                        //status --hide
                        //rtbStatusInfo.Document.Blocks.Add(new Paragraph(new Run($"Successfully saved all changes in {fileName} metadata.")));

                        if (checkBoxIncludeFileName.IsChecked == true)
                        {
                            //get file name
                            fileName = fileName;
                            //replace unwated characters and convert it to title case
                            fileName = ti.ToTitleCase(ReplaceCharacters(fileName));
                            //create new path
                            string newPath = path + "\\" + fileName;
                            //change file name
                            System.IO.File.Move(filePath, newPath);
                            //status --hide
                            //rtbStatusInfo.Document.Blocks.Add(new Paragraph(new Run($"Successfully changed file name to {fileName}.")));
                        }
                        //status --hide
                        //rtbStatusInfo.Document.Blocks.Add(new Paragraph(new Run("---Done---")));
                    }
                    System.Windows.MessageBox.Show("Done!", "", MessageBoxButton.OK, MessageBoxImage.None);
                }
                else
                    System.Windows.MessageBox.Show("Path is required.", "", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return null;
        }

        private string ReplaceCharacters(string name)
        {
            if (!string.IsNullOrEmpty(tbAdditionalStrings.Text))
            {
                string[] additinalWords = tbAdditionalStrings.Text.Split(';');
                for (int i = 0; i < additinalWords.Length; i++)
                    name = name.Replace(additinalWords[i], string.Empty);
            }
            //to lower case
            name = name.TrimEnd().ToLower();
            //replace all polish characters
            if(checkBoxPolishCharacters.IsChecked == true)
            {
                //an array with all special polish characters
                string[] polishCharacters = new string[] { "ą", "ę", "ó", "ł", "ś", "ć", "ń", "ż", "ź" };
                //and an array to replace above characters with these alpha replacement
                string[] polishCharactersReplacement = new string[] { "a", "e", "o", "l", "s", "c", "n", "z", "z" };
                for (int i = 0; i < polishCharacters.Length; i++)
                    name = name.Replace(polishCharacters[i], polishCharactersReplacement[i]);
            }
            return name;
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            GetFiles(tbFolderPath.Text);
        }

        private void btnCoverPath_Click(object sender, RoutedEventArgs e)
        {
            //create file dialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            //set filter for images
            dlg.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            //show dialog
            dlg.ShowDialog();
            //fill cover path
            tbCoverPath.Text = dlg.FileName;
            if(!string.IsNullOrEmpty(dlg.FileName))
                imgCover.Source = new BitmapImage(new Uri(dlg.FileName));
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.MessageBox.Show(@"
                Version: 1.0.0
                Copyright © 2017 MJ Programming
                Homepage: http://www.prgr.pl/
            ", "", MessageBoxButton.OK);
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            Process.Start("http://www.prgr.pl/");
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            Process.Start("http://www.prgr.pl/");
        }
    }
}
